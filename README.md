# Monitoring Resource



## RAM

Untuk memonitor penggunaan memori RAM, bisa menggunakan command `free`. Contoh output:
```bash
               total        used        free      shared  buff/cache   available
Mem:         2030420      271292      172796         692     1586332     1581720
Swap:              0           0           0
```
command `free --mega` untuk menampilkan dalam format Megabyte (MB). 
```bash
               total        used        free      shared  buff/cache   available
Mem:            2079         275         179           0        1624        1622
Swap:              0           0           0
```
command `free -h` untuk menampilkan dalam format Gigabyte (GB)
```bash
               total        used        free      shared  buff/cache   available
Mem:           1.9Gi       292Mi       208Mi       0.0Ki       1.4Gi       1.5Gi
Swap:             0B          0B          0B
```
## CPU 
Untuk memonitor penggunaan CPU, bisa menggunakan command `top` atau `ps -aux`. Contoh output:
command `top` untuk menampilkan yang sedang berjalan.
Contoh output: 

![CPU](https://gitlab.com/dvndnazhr20/praktikum-sistem-operasi/-/raw/main/ss/top.cpu.png)


command `ps -aux` untuk menampilkan yang hanya sedang di eksekusi, contoh output:


![CPU](https://gitlab.com/dvndnazhr20/praktikum-sistem-operasi/-/raw/main/ss/ps.aux.png)

## STORAGE / HARDISK
Untuk memonitor penggunaan Storage, bisa menggunakan command `df` atau `lsblk`, Contoh output:

![STORAGE](https://gitlab.com/dvndnazhr20/praktikum-sistem-operasi/-/raw/main/ss/df-lsblk.storage.png)

## MANAJEMEN PROGRAM

## Monitor program yang berjalan
sama seperti tadi menggunakan `top` atau `htop` namun pada command `htop` hanya bisa digunakan dalam linux.
## Menghentikan program yang berjalan
 menggunakan command 
 ```
 kill <PID>
 ```
## Otomasi perintah dengan shell script
Jika, kita ingin otomasi pembuatan folder dan file di dalamnya, maka perintah yang digunakan adalah `mkdir`, `cd`, dan `touch`.
untuk membuat file shell script dengan menggunakan command `touch` contohnya:
```
 project-generator.sh
```
Setelah file shell script dibuat, edit file tersebut menggunakan teks editor yang ada dan menggunakan `vim` untuk mengedit filenya, contohnya:
```
vim project-generator.sh
```
Buka file scriptnya dengan text editor yang tadi menggunakan `vim` lalu isi text editor nya sesuai kebutuhan/keinginan, jangan lupa menggunakan bash seperti contoh:
```
#! /bin/bash
```
Selanjutnya tinggal memasukkan perintah-perintah bash yang akan diotomasi. Contohnya kita akan memasukkan perintah untuk membuat file dengan mengambil nama dari parameter `touch $1`.
```
#! /bin/bash
touch $1
```
Selanjutnya, ubah permission dari file tersebut menggunakan perintah
```
chmod +x namafile.sh
```
Jika keluar ouput permisson denied,maka kita perlu mengubah hak akses file tersebut dengan menggunakan chmod yaitu dengan command
`ls -l` (berfungsi untuk melihat hak akses dari file dan folder )
chmod diikuti dengan 3 angka (masing masing angka antara 0 sampai 7). 3 angka tersebut secara berurutan mempresentasikan permission atau izin dari owner, group, dan others
```
r -> read -> 4


w -> write -> 2


x -> execute -> 1
```

Setelah permission diubah, file bisa langsung dieksekusi menggunakan perintah `./` atau `bash`
```
./namafile.sh
atau
bash namafile.sh
```
### Penjadwalan eksekusi program dengan cron
Cron yaitu fitur di sistem operasi berbasis UNIX (Linux, Ubuntu, dan lain-lain) yang berfungsi untuk menjalankan tugas atau script secara otomatis.
Untuk membuat program cron yaitu dengan cara menggunakan command `crontab -e`
Untuk menambahkan program kita perlu masuk ke mode insert yaitu dengan cara `esc + i` lalu masukan command
`m h dom mon dow` 
```
* * *   *   *   /home/user/script.sh

```
m yaitu minute,h yaitu hour, dom yaitu day of month,dow yaitu day of week.

Jika sudah maka selanjutnya masuk ke mode visual dengan cara `esc + v` dan untuk menyimpan program cron tersebut yaitu dengan mengetikan `:w` (di mode visual) dan untuk keluar program yaitu mengetikan `:q`
dan untuk melihat program crontab telah dibuat yaitu dengan menginputkan command `crontab -l`

### Manajemen Network
1. Untuk mengakses sistem operasi dengan SSH adalah menggunakan command berikut:

->  Akses dengan ip
```
ssh <account>@<public-ip>

account = username
public-ip = ip public tujuan
```
2. Untuk dapat memonitor Network kita perlu memasukan command  `netstat` (Network Statistics)

![Manajemen](https://gitlab.com/dvndnazhr20/praktikum-sistem-operasi/-/raw/main/ss/netstat.manajemen.png)

3. Mengoperasikan HTTP client
adalah perangkat lunak yang terutama digunakan untuk memungkinkan kita mengunduh file dari Internet. Alasan utama menggunakan klien HTTP umumnya untuk mengunduh file, tetapi ini juga dapat digunakan jika kita ingin melakukan debug atau berinteraksi dengan server web atau beberapa server web.Untuk mengoperasikan HTTP client yaitu dengan menggunakan perintah `curl` atau `wget`.


- wget adalah mengunduh file tunggal dan menyimpannya di direktori working Anda saat ini.
```
wget [option] [URL]
```
- Curl command adalah command yang tersedia di sebagian besar sistem berbasis Unix. Curl merupakan singkatan dari “Client URL”.

```
curl [Options] [URL]

```
### MANAJEMEN FILE DAN FOLDER

Ada beberapa perintah untuk melakukan CRUD file dan folder diantaranya:


1. `cd` untuk berpindah direktori / folder
2. `ls` untuk melihat isi direktori / folder
3. `mkdir` untuk membuat folder
4. `touch` untuk membuat file
5. `vim` untuk mengedit isi file
6. `rmdir` untuk menghapus folder
7. `rm` untuk menghapus file / folder
8. `mv` untuk memindahkan file atau folder
9. `cp` untuk menyalin file atau folder


![MANAJEMEN](https://gitlab.com/dvndnazhr20/praktikum-sistem-operasi/-/raw/main/ss/navigasi.png)

### MANAJEMEN OWNERSHIP
Untuk mengubah izin file atau folder bisa menggunakan command `chmod`. Untuk mengubah hak milik bisa menggunakan command `chown`. 
CHMOD (Change Mode) adalah merubah mode akses dari suatu file atau direktori, CHOWN (Change Owner) digunakan untuk mengubah pemilik dan grup pada sebuah file dan folder, berikut contoh penggunaannya:
```
chown [user:group] <file>
chown digunakan untuk memodifikasi owner dari suatu file.
[user:group] berisi nama user/group mana yang akan menjadi ownernya.
<file> berisi nama file yang akan diubah ownernya
```
```
chmod [option] <file>
chmod digunakan untuk memodifikasi permission suatu file.
[option] berisi perintah untuk mengubah permission
<file> berisi nama file yang akan diubah permissionnya.
ls -l berfungsi sebgai melihat hak akses dari file dan folder
```
![MANAJEMEN](https://gitlab.com/dvndnazhr20/praktikum-sistem-operasi/-/raw/main/ss/chmod.png)

- Pencarian File Dan Folder

Untuk mencari file dan folder bisa menggunakan command `find` atau `grep`. Contoh penggunaan find:
```
find <path> <conditions> <actions>
```
```
grep [options] pattern [file ...]

```
- Kompresi Data

Ada beberapa perintah untuk melakukan kompresi data, namun disini kita akan membahas 2 yaitu dengan menggunakan command `tar` dan `gzip`.
1. TAR  

`tar -cf` (untuk membuat file.tar)
`tar -xf` (untuk membuka file.tar)

```
tar [options] [output] [file or folder]

tar -cf output.tar file.txt
```
Untuk mengekstrak file yang dikompresi `tar` bisa dengan cara
```
tar -xf output.tar

```
2. GZIP 

Zip merupakan format file arsip yang digunakan secara luas untuk mengompresi atau memampatkan satu atau beberapa file bersama-sama menjadi ke dalam satu lokasi
, Contoh yaitu dengan menggunakan file abc.JPG dengan total ukuran 40k.

```
gzip [options] [file]

```
Untuk mengekstrak file yang dikompresi gzip bisa dengan cara

```
gunzip [options] [filenames]

```
[options] diisi dengan options yang akan digunakan.

[filenames] diisi dengan nama file yang akan dicompress.

### MEMBUAT PROGRAM CLI (SHELLSCRIPT)
Langkah pertama membuat `file.sh` terlebih dahulu dengan command:
```
touch [nama file]
```
Setelah Itu kita lihat apakah file project-generator.sh telah terbuat dengan menggunakan command

```
ls

```
dan masuk kedalam file project-generator.sh dengan command

```
vim [nama file]

```

outputnya:

![SHELLSCRIPT](https://gitlab.com/dvndnazhr20/praktikum-sistem-operasi/-/raw/main/ss/output.png)

jika telah masuk ke file tersebut kita perlu mengubah mode ke mode insert dengan menggunakan command

```
ESC + I

```
lalu inputkan kode sesuai kebutuhan,setelah diinput yaitu menyimpan kodingan tersebut dengan cara masuk ke mode visul menggunakan command

```
ESC + V

```
lalu inputkan `:w` untuk menyimpan dan `:q` untuk keluar vim

dan disini saya membuat program pendeteksi bilangan

![SHELLSCRIPT](https://gitlab.com/dvndnazhr20/praktikum-sistem-operasi/-/raw/main/ss/shell_script.png)

lalu kita jalankan dengan menggunakan command `./namafile` dan ini outputnya :

![SHELLSCRIPT](https://gitlab.com/dvndnazhr20/praktikum-sistem-operasi/-/raw/main/ss/12.png)

### DEMO CLI KE DALAM PLATFORM YOUTUBE 

[Link Demo](https://youtu.be/p3LPh3YApU0)

### CHALLENGE MAZE GAME

untuk maze game sendiri saya bikin quiz 

![GAME](https://gitlab.com/dvndnazhr20/praktikum-sistem-operasi/-/raw/main/ss/game1.png)

![GAME](https://gitlab.com/dvndnazhr20/praktikum-sistem-operasi/-/raw/main/ss/game2.png)

![GAME](https://gitlab.com/dvndnazhr20/praktikum-sistem-operasi/-/raw/main/ss/game3.png)

![GAME](https://gitlab.com/dvndnazhr20/praktikum-sistem-operasi/-/raw/main/ss/game4.png)















